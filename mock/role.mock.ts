import { defineMock } from "./base";

export default defineMock([
  {
    url: "/sys/role/page",
    method: ["GET"],
    body:
      {
        "code": 200,
        "message": "操作成功",
        "data": {
          "records": [
            {
              "id": 1,
              "name": "管理员",
              "roleKey": "ROOT",
              "dataScope": "0",
              "description": "超级管理员",
              "userCount": 1,
              "createTime": "2024-12-02 10:57:02",
              "status": "1",
              "sort": 0,
              "valid": 1
            },
            {
              "id": 2,
              "name": "测试",
              "roleKey": "TEST",
              "dataScope": "1",
              "description": "测试角色",
              "userCount": 1,
              "createTime": "2024-12-02 15:21:49",
              "status": "1",
              "sort": 0,
              "valid": 1
            },
            {
              "id": 3,
              "name": "c角色",
              "roleKey": "ADD",
              "dataScope": "3",
              "description": null,
              "userCount": null,
              "createTime": "2024-12-23 17:41:40",
              "status": "1",
              "sort": 1,
              "valid": 1
            }
          ],
          "total": 3,
          "size": 10,
          "current": 1,
          "pages": 1
        }
      }
    ,
  },
]);
