import { defineMock } from "./base";

export default defineMock([
  {
    url: "users/me",
    method: ["GET"],
    body: {
      code: 200,
      data: {
        userId: 2,
        nickname: "系统管理员",
        username: "admin",
        avatar:
          "https://foruda.gitee.com/images/1723603502796844527/03cdca2a_716974.gif?imageView2/1/w/80/h/80",
        roles: ["ROOT"],
        permissions: [
          "sys:menu:delete",
          "sys:dept:edit",
          "sys:dict_type:add",
          "sys:dict:edit",
          "sys:dict:delete",
          "sys:dict_type:edit",
          "sys:menu:add",
          "sys:user:add",
          "sys:role:edit",
          "sys:dept:delete",
          "sys:user:edit",
          "sys:user:delete",
          "sys:user:password:reset",
          "sys:dept:add",
          "sys:role:delete",
          "sys:dict_type:delete",
          "sys:menu:edit",
          "sys:dict:add",
          "sys:role:add",
          "sys:user:query",
          "sys:user:export",
          "sys:user:import",
        ],
      },
      message: "一切ok",
    },
  },
  {
    url: "/sys/user/page",
    method: ["GET"],
    body:
      {
        "code": 200,
        "message": "操作成功",
        "data": {
          "records": [
            {
              "id": 1,
              "username": "admin",
              "password": "$2a$10$Wr0wX8Ueiox9ndNXsrx8JOfe3sc6QIsTW4JW/rPR6.7iAzrdKLYSe",
              "deptId": 1,
              "deptName": "Harry科技",
              "postIds": [],
              "icon": "http://static.tech-harry.cn/typora/1706767923416-a83e9ea2-556d-4cfb-9f4c-709fe5a7e7c0.png",
              "email": "183865800@qq.com",
              "phone": "17777777777",
              "nickName": "Harry",
              "sex": "0",
              "note": null,
              "status": "1",
              "loginTime": 1732094753000,
              "loginIp": "192.168.31.7",
              "createTime": "2019-09-29 13:55:30",
              "createBy": null,
              "modifyTime": "2024-12-20 14:10:38",
              "modifyBy": "admin",
              "valid": 1,
              "roleIds": null
            },
            {
              "id": 101,
              "username": "harry",
              "password": "$2a$10$Wr0wX8Ueiox9ndNXsrx8JOfe3sc6QIsTW4JW/rPR6.7iAzrdKLYSe",
              "deptId": 2,
              "deptName": "研发部",
              "postIds": [
                "3"
              ],
              "icon": "",
              "email": "183865800@qq.com",
              "phone": "17777777777",
              "nickName": "Harry测试",
              "sex": "1",
              "note": null,
              "status": "1",
              "loginTime": 1732095144000,
              "loginIp": "192.168.31.7",
              "createTime": "2019-09-29 13:55:30",
              "createBy": null,
              "modifyTime": "2024-12-02 10:59:49",
              "modifyBy": "harry",
              "valid": 1,
              "roleIds": null
            },
            {
              "id": 102,
              "username": "chanpin_1",
              "password": "$2a$10$cUXEAaWfp80uu0DjH0SUYeXLf17iBdcQ6WL1X/4.2MRngcHH.jD9a",
              "deptId": 6,
              "deptName": "产品2部",
              "postIds": null,
              "icon": null,
              "email": "17777777777@163.com",
              "phone": "17777777777",
              "nickName": "产品1部",
              "sex": "0",
              "note": null,
              "status": "1",
              "loginTime": null,
              "loginIp": null,
              "createTime": "2024-12-27 15:11:05",
              "createBy": null,
              "modifyTime": "2024-12-31 09:07:02",
              "modifyBy": null,
              "valid": 1,
              "roleIds": null
            }
          ],
          "total": 3,
          "size": 10,
          "current": 1,
          "pages": 1
        }
      }
    ,
  },
]);
