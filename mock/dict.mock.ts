import { defineMock } from "./base";

export default defineMock([
  {
    url: "/sys/dict/list",
    method: ["GET"],
    body: {
    "code": 200,
    "message": "操作成功",
    "data": [
      {
        "name": "用户性别",
        "type": "sys_user_sex",
        "data": [
          {
            "value": "0",
            "label": "男",
            "tagType": "success"
          },
          {
            "value": "1",
            "label": "女",
            "tagType": "warning"
          },
          {
            "value": "2",
            "label": "未知",
            "tagType": "info"
          }
        ]
      },
      {
        "name": "菜单状态",
        "type": "sys_show_hide",
        "data": [
          {
            "value": "1",
            "label": "显示",
            "tagType": "primary"
          },
          {
            "value": "0",
            "label": "隐藏",
            "tagType": "danger"
          }
        ]
      },
      {
        "name": "系统开关",
        "type": "sys_normal_disable",
        "data": [
          {
            "value": "1",
            "label": "正常",
            "tagType": "primary"
          },
          {
            "value": "0",
            "label": "停用",
            "tagType": "danger"
          }
        ]
      },
      {
        "name": "任务状态",
        "type": "sys_job_status",
        "data": [
          {
            "value": "1",
            "label": "正常",
            "tagType": "primary"
          },
          {
            "value": "1",
            "label": "暂停",
            "tagType": "danger"
          }
        ]
      },
      {
        "name": "任务分组",
        "type": "sys_job_group",
        "data": [
          {
            "value": "DEFAULT",
            "label": "默认",
            "tagType": "primary"
          },
          {
            "value": "SYSTEM",
            "label": "系统",
            "tagType": "primary"
          }
        ]
      },
      {
        "name": "系统是否",
        "type": "sys_yes_no",
        "data": [
          {
            "value": "Y",
            "label": "是",
            "tagType": "primary"
          },
          {
            "value": "N",
            "label": "否",
            "tagType": "danger"
          }
        ]
      },
      {
        "name": "通知类型",
        "type": "sys_notice_type",
        "data": [
          {
            "value": "1",
            "label": "通知",
            "tagType": "warning"
          },
          {
            "value": "2",
            "label": "公告",
            "tagType": "success"
          }
        ]
      },
      {
        "name": "通知状态",
        "type": "sys_notice_status",
        "data": [
          {
            "value": "0",
            "label": "正常",
            "tagType": "primary"
          },
          {
            "value": "1",
            "label": "关闭",
            "tagType": "danger"
          }
        ]
      },
      {
        "name": "操作类型",
        "type": "sys_oper_type",
        "data": [
          {
            "value": "1",
            "label": "新增",
            "tagType": "info"
          },
          {
            "value": "2",
            "label": "修改",
            "tagType": "info"
          },
          {
            "value": "3",
            "label": "删除",
            "tagType": "danger"
          },
          {
            "value": "4",
            "label": "授权",
            "tagType": "primary"
          },
          {
            "value": "5",
            "label": "导出",
            "tagType": "warning"
          },
          {
            "value": "6",
            "label": "导入",
            "tagType": "warning"
          },
          {
            "value": "7",
            "label": "强退",
            "tagType": "danger"
          },
          {
            "value": "8",
            "label": "生成代码",
            "tagType": "warning"
          },
          {
            "value": "9",
            "label": "清空数据",
            "tagType": "danger"
          }
        ]
      },
      {
        "name": "系统状态",
        "type": "sys_common_status",
        "data": [
          {
            "value": "1",
            "label": "成功",
            "tagType": "primary"
          },
          {
            "value": "0",
            "label": "失败",
            "tagType": "danger"
          }
        ]
      },
      {
        "name": "用户岗位",
        "type": "sys_user_post",
        "data": [
          {
            "value": "1",
            "label": "董事长",
            "tagType": null
          },
          {
            "value": "2",
            "label": "项目经理",
            "tagType": null
          },
          {
            "value": "3",
            "label": "人力资源",
            "tagType": null
          },
          {
            "value": "4",
            "label": "产品经理",
            "tagType": null
          },
          {
            "value": "5",
            "label": "普通员工",
            "tagType": null
          }
        ]
      },
      {
        "name": "菜单类型",
        "type": "sys_menu_type",
        "data": [
          {
            "value": "0",
            "label": "目录",
            "tagType": "primary"
          },
          {
            "value": "1",
            "label": "菜单",
            "tagType": "success"
          },
          {
            "value": "2",
            "label": "按钮",
            "tagType": "warning"
          },
          {
            "value": "4",
            "label": "外链",
            "tagType": "primary"
          }
        ]
      }
    ]
    },
  },
  {
    url: "/sys/dict/page",
    method: ["GET"],
    body:{
      "code": 200,
      "message": "操作成功",
      "data": {
        "records": [
          {
            "id": 1,
            "name": "用户性别",
            "type": "sys_user_sex",
            "status": "1",
            "remark": "用户性别",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:12",
            "valid": 1
          },
          {
            "id": 2,
            "name": "菜单状态",
            "type": "sys_show_hide",
            "status": "1",
            "remark": "菜单状态",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:14",
            "valid": 1
          },
          {
            "id": 3,
            "name": "系统开关",
            "type": "sys_normal_disable",
            "status": "1",
            "remark": "系统开关",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:15",
            "valid": 1
          },
          {
            "id": 4,
            "name": "任务状态",
            "type": "sys_job_status",
            "status": "1",
            "remark": "任务状态",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:16",
            "valid": 1
          },
          {
            "id": 5,
            "name": "任务分组",
            "type": "sys_job_group",
            "status": "1",
            "remark": "任务分组",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:17",
            "valid": 1
          },
          {
            "id": 6,
            "name": "系统是否",
            "type": "sys_yes_no",
            "status": "1",
            "remark": "系统是否",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:18",
            "valid": 1
          },
          {
            "id": 7,
            "name": "通知类型",
            "type": "sys_notice_type",
            "status": "1",
            "remark": "通知类型",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:19",
            "valid": 1
          },
          {
            "id": 8,
            "name": "通知状态",
            "type": "sys_notice_status",
            "status": "1",
            "remark": "通知状态",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:21",
            "valid": 1
          },
          {
            "id": 9,
            "name": "操作类型",
            "type": "sys_oper_type",
            "status": "1",
            "remark": "操作类型",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:22",
            "valid": 1
          },
          {
            "id": 10,
            "name": "系统状态",
            "type": "sys_common_status",
            "status": "1",
            "remark": "系统状态",
            "createTime": "2024-12-02 14:51:06",
            "modifyTime": "2024-12-02 14:51:24",
            "valid": 1
          }
        ],
        "total": 12,
        "size": 10,
        "current": 1,
        "pages": 2
      }
    },
  },
]);
