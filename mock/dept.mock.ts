import { defineMock } from "./base";

export default defineMock([
  {
    url: "/sys/dept/options",
    method: ["GET"],
    body:
      {
        "code": 200,
        "message": "操作成功",
        "data": [
          {
            "value": 1,
            "label": "Harry技术",
            "children": [
              {
                "value": 2,
                "label": "研发部"
              },
              {
                "value": 3,
                "label": "营销部"
              },
              {
                "value": 4,
                "label": "产品部",
                "children": [
                  {
                    "value": 5,
                    "label": "产品1部"
                  },
                  {
                    "value": 6,
                    "label": "产品2部"
                  }
                ]
              }
            ]
          }
        ]
      },
  },
]);
