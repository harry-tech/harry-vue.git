import { defineMock } from "./base";

export default defineMock([
  {
    url: "/sys/config/page",
    method: ["GET"],
    body:{
      "code": 200,
      "message": "操作成功",
      "data": {
        "records": [
          {
            "id": 1,
            "configType": "Y",
            "configKey": "SYS_CAPTCHA_IMG",
            "configValue": "false",
            "configName": "系统验证码开关",
            "status": "1",
            "remark": "系统验证码开关",
            "createTime": "2024-12-20 16:00:45",
            "modifyTime": "2024-12-20 16:00:45",
            "valid": 1
          }
        ],
        "total": 1,
        "size": 10,
        "current": 1,
        "pages": 1
      }
    },
  },
]);
