- 【报错】Deprecation Warning: The legacy JS API is deprecated and will be removed in Dart Sass 2.0.0
最优解法是使用新版 api 解决根本问题：

```ts
// vite.config.ts
export default defineConfig({
    css: {
        preprocessorOptions: {
            scss: {
                api: 'modern-compiler', // or 'modern'
            },
        },
    },
})
```


                         