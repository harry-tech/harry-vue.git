import request from "@/utils/request";

const USER_BASE_URL = "/sys/user/profile";

const UserProfileAPI = {

  /** 获取个人中心用户信息 */
  getProfile() {
    return request<any, UserProfileVO>({
      url: `${USER_BASE_URL}`,
      method: "get",
    });
  },

  /** 修改个人中心用户信息 */
  updateProfile(data: UserProfileForm) {
    return request({
      url: `${USER_BASE_URL}`,
      method: "put",
      data: data,
    });
  },

  updateAvatar(data: UpdateIconForm) {
    return request({
      url: `${USER_BASE_URL}/avatar`,
      method: "post",
      data: data,
    });
  },

  /** 修改个人中心用户密码 */
  changePassword(data: PasswordChangeForm) {
    return request({
      url: `${USER_BASE_URL}/updatePassword`,
      method: "put",
      data: data,
    });
  },

  /**
   *   发送手机/邮箱验证码
   *
   * @param contact 联系方式  手机号/邮箱
   * @param contactType 联系方式类型 phone:手机;EMAIL:邮箱
   */
  sendVerificationCode(contact: string, contactType: string) {
    return request({
      url: `${USER_BASE_URL}/sendVerificationCode`,
      method: "post",
      params: { contact: contact, contactType: contactType },
    });
  },

  /** 绑定个人中心用户手机 */
  bindPhone(data: PhoneBindingForm) {
    return request({
      url: `${USER_BASE_URL}/bindPhone`,
      method: "post",
      data: data,
    });
  },

  /** 绑定个人中心用户邮箱 */
  bindEmail(data: EmailBindingForm) {
    return request({
      url: `${USER_BASE_URL}/bindEmail`,
      method: "post",
      data: data,
    });
  },
}
export default UserProfileAPI;

/** 个人中心用户信息 */
export interface UserProfileVO {
  /** 用户名 */
  username?: string;

  /** 昵称 */
  nickName?: string;

  /** 头像URL */
  icon?: string;

  /** 性别 */
  sex?: string;

  /** 手机号 */
  phone?: string;

  /** 邮箱 */
  email?: string;

  /** 部门名称 */
  deptName?: string;

  /** 角色名称，多个使用英文逗号(,)分割 */
  roles?: string;

  /** 创建时间 */
  createTime?: Date;
}

/** 个人中心用户信息表单 */
export interface UserProfileForm {
  /** 昵称 */
  nickName?: string;
  /** 性别 */
  sex?: string;
}

/** 修改密码表单 */
export interface PasswordChangeForm {
  /** 原密码 */
  oldPassword?: string;
  /** 新密码 */
  newPassword?: string;
  /** 确认新密码 */
  confirmPassword?: string;
}

/** 修改手机表单 */
export interface PhoneBindingForm {
  /** 手机号 */
  phone?: string;
  /** 验证码 */
  code?: string;
}

/** 修改邮箱表单 */
export interface EmailBindingForm {
  /** 邮箱 */
  email?: string;
  /** 验证码 */
  code?: string;
}

/** 修改头像表单 */
export interface UpdateIconForm {
  /** 头像 */
  icon?: string;

}
