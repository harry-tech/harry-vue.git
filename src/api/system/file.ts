import request from "@/utils/request";

const FILE_BASE_URL = "/sys/file";

const FileAPI = {
    /** 获取文件管理分页数据 */
    getPage(queryParams?: FilePageQuery) {
        return request<any, PageResult<FilePageVO[]>>({
            url: `${FILE_BASE_URL}/page`,
            method: "get",
            params: queryParams,
        });
    },
    /**
     * 获取文件管理表单数据
     *
     * @param id FileID
     * @returns File表单数据
     */
    getFormData(id: number) {
        return request<any, FileForm>({
            url: `${FILE_BASE_URL}/${id}`,
            method: "get",
        });
    },

    /** 添加文件管理*/
    add(data: FileForm) {
        return request({
            url: `${FILE_BASE_URL}`,
            method: "post",
            data: data,
        });
    },

    /**
     * 更新文件管理
     *
     * @param id FileID
     * @param data File表单数据
     */
     update(data: FileForm) {
        return request({
            url: `${FILE_BASE_URL}`,
            method: "put",
            data: data,
        });
    },

    /**
     * 批量删除文件管理，多个以英文逗号(,)分割
     *
     * @param ids 文件管理ID字符串，多个以英文逗号(,)分割
     */
     deleteByIds(ids: string) {
        return request({
            url: `${FILE_BASE_URL}/${ids}`,
            method: "delete",
        });
    }
}

export default FileAPI;

/** 文件管理分页查询参数 */
export interface FilePageQuery extends PageQuery {
    /** 文件名 */
    fileName?: string;
    /** 文件存储桶名称 */
    bucketName?: string;
    /** 桶类型 aliyun,minio */
    bucketType?: string;
}

/** 文件管理表单对象 */
export interface FileForm {
    /** 主键ID */
    id?: number;
    /** 文件名 */
    fileName?:  string;
    /** 原始文件名 */
    original?:  string;
    /** 文件存储桶名称 */
    bucketName?:  string;
    /** 桶类型 aliyun,minio */
    bucketType?:  string;
    /** 文件类型 */
    type?:  string;
    /** 文件大小 */
    fileSize?:  number;
    /** 访问地址 */
    domain?:  string;
}

/** 文件管理分页对象 */
export interface FilePageVO {
    /** 主键ID */
    id?: number;
    /** 文件名 */
    fileName?: string;
    /** 原始文件名 */
    original?: string;
    /** 文件存储桶名称 */
    bucketName?: string;
    /** 桶类型 aliyun,minio */
    bucketType?: string;
    /** 文件类型 */
    type?: string;
    /** 文件大小 */
    fileSize?: number;
    /** 访问地址 */
    domain?: string;
    /** 创建时间 */
    createTime?: Date;
}
