import request from "@/utils/request";

const DICT_DATA_BASE_URL = "/sys/dict/data";

const DictDataAPI = {
  /**
   * 获取字典分页列表
   *
   * @param queryParams 查询参数
   * @returns 字典分页结果
   */
  getPage(queryParams: DictDataPageQuery) {
    return request<any, PageResult<DictDataPageVO[]>>({
      url: `${DICT_DATA_BASE_URL}/page`,
      method: "get",
      params: queryParams
    });
  },

  /**
   * 获取字典数据表单
   *
   * @param id 字典ID
   * @returns 字典数据表单
   */
  getFormData(id: number) {
    return request<any, ResponseData<DictDataForm>>({
      url: `${DICT_DATA_BASE_URL}/${id}`,
      method: "get"
    });
  },

  /**
   * 新增字典数据
   *
   * @param data 字典数据
   */
  add(data: DictDataForm) {
    return request({
      url: `${DICT_DATA_BASE_URL}`,
      method: "post",
      data: data
    });
  },

  /**
   * 修改字典数据
   *
   * @param id 字典ID
   * @param data 字典数据
   */
  update( data: DictDataForm) {
    return request({
      url: `${DICT_DATA_BASE_URL}`,
      method: "put",
      data: data
    });
  },

  /**
   * 删除字典
   *
   * @param ids 字典ID，多个以英文逗号(,)分隔
   */
  deleteByIds(ids: string) {
    return request({
      url: `${DICT_DATA_BASE_URL}/${ids}`,
      method: "delete"
    });
  },

  /**
   * 获取字典的数据项
   *
   * @param dictType 字典编码
   * @returns 字典数据项
   */
  getOptions(dictType: string) {
    return request<any, OptionType[]>({
      url: `${DICT_DATA_BASE_URL}/${dictType}/options`,
      method: "get"
    });
  }
};

export default DictDataAPI;

/**
 * 字典查询参数
 */
export interface DictDataPageQuery extends PageQuery {
  /** 关键字(字典数据值/标签) */
  keywords?: string;

  /** 字典编码 */
  dictType?: string;
}

/**
 * 字典分页对象
 */
export interface DictDataPageVO {
  /**ID */
  id?: number;

  /**字典排序 */
  dictSort?: number;

  /**字典标签 */
  dictLabel?: string;

  /**字典键值 */
  dictValue?: string;

  /**字典类型 */
  dictType?: string;

  /**样式属性（其他样式扩展） */
  cssClass?: string;

  /**表格回显样式 */
  listClass?: string;

  /**是否默认，Y:是 N:否 */
  isDefault?: string;

  /**启用状态，0:禁用 1:启用 */
  status?: string;
}

/**
 * 字典
 */
export interface DictDataForm {
  /**ID */
  id?: number;

  /**字典排序 */
  dictSort?: number;

  /**字典标签 */
  dictLabel?: string;

  /**字典键值 */
  dictValue?: string;

  /**字典类型 */
  dictType?: string;

  /**样式属性（其他样式扩展） */
  cssClass?: string;

  /**表格回显样式 */
  listClass?:  "success" | "warning" | "info" | "primary" | "danger" | undefined;

  /**是否默认，Y:是 N:否 */
  isDefault?: string;

  /**启用状态，0:禁用 1:启用 */
  status?: string;

}
