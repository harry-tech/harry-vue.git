import request from "@/utils/request";

class AuthAPI {
  /** 登录 接口*/
  static login(data: LoginData) {
    const formData = new FormData();
    formData.append("username", data.username);
    formData.append("password", data.password);
    formData.append("uuid", data.uuid);
    formData.append("captchaCode", data.captchaCode);
    return request<any, LoginResult>({
      url: "/auth/login",
      method: "post",
      data: formData,
    });
  }

  /** 注销 接口*/
  static logout() {
    return request({
      url: "/auth/logout",
      method: "post",
    });
  }

  /** 获取验证码 接口*/
  static getCaptcha() {
    return request<any, CaptchaResult>({
      url: "/auth/captcha",
      method: "get",
    });
  }
}

export default AuthAPI;

/** 登录请求参数 */
export interface LoginData {
  /** 用户名 */
  username: string;
  /** 密码 */
  password: string;
  /** 验证码缓存key */
  uuid: string;
  /** 验证码 */
  captchaCode: string;
}

/** 登录响应 */
export interface LoginResult {
  /** 访问token */
  token?: string;
  /** 过期时间(单位：毫秒) */
  expiration?: number;
  /** 刷新token */
  refreshToken?: string;
  /** token 类型 */
  tokenType?: string;
}

/** 验证码响应 */
export interface CaptchaResult {
  /** 验证码缓存key */
  uuid: string;
  /** 验证码图片Base64字符串 */
  img: string;
}
