import request from "@/utils/request";

class UserAPI {
  /**
   * 获取当前登录用户信息
   *
   * @returns 登录用户昵称、头像信息，包括角色和权限
   */
  static getInfo() {
    return request<any, UserInfo>({
      url: "/auth/info",
      method: "get",
    });
  }
}

export default UserAPI;

/** 登录用户信息 */
export interface UserInfo {
  /** 用户名 */
  username?: string;

  /** 详细信息 */
  detailInfo?: dataInfo;

  /** 角色 */
  roles: string[];

  /** 权限 */
  permissions: string[];
}
interface dataInfo {
  /**用户ID */
  id?: number;

  /**用户名 */
  username?: string;

  /**所属部门 */
  deptId?: number;

  /**所属部门名称 */
  deptName?: string;

  /**岗位组 */
  postIds?: Record<string, unknown>[];

  /**头像 */
  icon?: string;

  /**邮箱 */
  email?: string;

  /**手机号 */
  phone?: string;

  /**昵称 */
  nickName?: string;

  /**性别，0:男 1:女 2:未知 */
  sex?: string;

  /**备注信息 */
  note?: string;

  /**启用状态，0:禁用 1:启用 */
  status?: string;

  /**最后登录时间 */
  loginTime?: Record<string, unknown>;

  /**最后登陆IP */
  loginIp?: string;

  /**创建时间 */
  createTime?: Record<string, unknown>;

}
