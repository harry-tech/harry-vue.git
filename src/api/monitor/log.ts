import request from "@/utils/request";

const SYSLOG_BASE_URL = "/monitor/log";

const SysLogAPI = {
  /** 获取系统日志分页数据 */
  getPage(queryParams?: SysLogPageQuery) {
    return request<any, PageResult<SysLogPageVO[]>>({
      url: `${SYSLOG_BASE_URL}/page`,
      method: "get",
      params: queryParams,
    });
  },
  /**
   * 获取系统日志表单数据
   *
   * @param id SysLogID
   * @returns SysLog表单数据
   */
  getFormData(id: number) {
    return request<any, SysLogForm>({
      url: `${SYSLOG_BASE_URL}/${id}`,
      method: "get",
    });
  },

  /** 添加系统日志*/
  add(data: SysLogForm) {
    return request({
      url: `${SYSLOG_BASE_URL}`,
      method: "post",
      data: data,
    });
  },

  /**
   * 更新系统日志
   *
   * @param id SysLogID
   * @param data SysLog表单数据
   */
  update(data: SysLogForm) {
    return request({
      url: `${SYSLOG_BASE_URL}`,
      method: "put",
      data: data,
    });
  },

  /**
   * 批量删除系统日志，多个以英文逗号(,)分割
   *
   * @param ids 系统日志ID字符串，多个以英文逗号(,)分割
   */
  deleteByIds(ids: string) {
    return request({
      url: `${SYSLOG_BASE_URL}/${ids}`,
      method: "delete",
    });
  }
}

export default SysLogAPI;

/** 系统日志分页查询参数 */
export interface SysLogPageQuery extends PageQuery {
  /** 模块标题 */
  title?:  string;
  /** 方法名称 */
  method?:  string;
  /** 操作人员 */
  username?:  string;
}

/** 系统日志表单对象 */
export interface SysLogForm {
  /** 主键 */
  id?:  number;
  /** 模块标题 */
  title?:  string;
  /** 方法名称 */
  method?:  string;
  /** 请求方式 */
  requestMethod?:  string;
  /** 操作类别（0其它 1后台用户 2 手机端用户） */
  operatorType?:  string;
  /** 操作人员 */
  username?:  string;
  /** 请求URL */
  url?:  string;
  /** 主机地址 */
  ip?:  string;
  /** 操作地点 */
  location?:  string;
  /** 请求参数 */
  param?:  string;
  /** 返回参数 */
  jsonResult?:  string;
  /** 操作状态（0正常 1异常） */
  status?:  string;
  /** 错误消息 */
  errorMsg?:  string;
  /** 执行时间(ms) */
  executionTime?:  number;
  /** 浏览器 */
  browser?:  string;
  /** 浏览器版本 */
  browserVersion?:  string;
  /** 终端系统 */
  os?:  string;
  /** 创建时间 */
  createTime?: Date;
}

/** 系统日志分页对象 */
export interface SysLogPageVO {
  /** 主键 */
  id?: number;
  /** 模块标题 */
  title?: string;
  /** 方法名称 */
  method?: string;
  /** 请求方式 */
  requestMethod?: string;
  /** 操作类别（0其它 1后台用户 2手机端用户） */
  operatorType?: string;
  /** 操作人员 */
  username?: string;
  /** 请求URL */
  url?: string;
  /** 主机地址 */
  ip?: string;
  /** 操作地点 */
  location?: string;
  /** 请求参数 */
  param?: string;
  /** 返回参数 */
  jsonResult?: string;
  /** 操作状态（0正常 1异常） */
  status?: string;
  /** 错误消息 */
  errorMsg?: string;
  /** 执行时间(ms) */
  executionTime?: number;
  /** 浏览器 */
  browser?: string;
  /** 浏览器版本 */
  browserVersion?: string;
  /** 终端系统 */
  os?: string;
  /** 创建时间 */
  createTime?: Date;
}
