## 环境准备

| 环境                 | 名称版本                                                     | 下载地址                                                     |
| -------------------- | :----------------------------------------------------------- | ------------------------------------------------------------ |
| **开发工具**         | VSCode    | [下载](https://code.visualstudio.com/Download)           |
| **运行环境**         | Node ≥18 (其中 20.6.0 版本不可用)    | [下载](http://nodejs.cn/download)                        |


## 项目启动

```bash
# 克隆代码
git clone https://gitee.com/harry-tech/harry-vue.git

# 进入项目目录
cd harry-vue

# 安装 pnpm
npm install pnpm -g

# 设置镜像源(可忽略)
pnpm config set registry https://registry.npmmirror.com

# 安装依赖
pnpm install

# 启动运行
pnpm run dev
```


## 本地Mock

项目同时支持在线和本地 Mock 接口，默认使用线上接口，如需替换为 Mock 接口，修改文件 `.env.development` 的 `VITE_MOCK_DEV_SERVER` 为  `true` **即可**。

## 后端接口

1. 获取基于 `Java` 和 `SpringBoot` 开发的后端。   **Gitee仓库地址:** https://gitee.com/harry-tech/harry.git
2. 根据后端工程的说明文档 [README.md](https://gitee.com/harry-tech/harry/blob/v3/README.md) 完成本地启动。
3. 修改 `.env.development` 文件中的 `VITE_APP_API_URL` 的值，更改为 http://localhost:9000 即可。


#### 示例演示

![暗黑模式](https://foruda.gitee.com/images/1736158065359839109/2811b37c_1071933.gif "暗黑_20241231_121444_compressed.gif")
